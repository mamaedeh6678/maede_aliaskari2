import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeRoutingModule } from './employee-routing.module';
import { ListComponent } from './list/list.component';
import { RegisterComponent } from './register/register.component';
import {TableModule} from 'primeng/table';
import {ButtonModule} from 'primeng/button';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [ListComponent, RegisterComponent],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    TableModule,
    FormsModule,
    ButtonModule,
  ]
})
export class EmployeeModule { }
