import { Component, OnInit } from '@angular/core';
import {RestService} from '../../rest.service';
import {Employee} from '../list/employee';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private rest: RestService) {
  }
  employee: Employee;

  ngOnInit() {
    this.employee = {id: null, name: '', salary: null, age: null};

  }

  saveEmployee() {
    this.rest.saveEmployee(this.employee).subscribe(response => {
      console.log('server response', response);
    });
  }
}

