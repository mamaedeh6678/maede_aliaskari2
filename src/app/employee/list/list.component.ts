import { Component, OnInit } from '@angular/core';
import {Employee} from './employee';
import {RestService} from '../../rest.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  constructor(private rest: RestService) { }
  employeeList = [];
  employee: Employee;


  ngOnInit() {

    this.employee = {id: null, name: '', salary: null, age: null };
    this.getAllEmployees();
  }

  getAllEmployees() {
    this.rest.getEmployees().subscribe(r => {
      console.log('employee list', r);
      this.employeeList = r.data;
    });
  }

  deleteEmployee(id: number) {
    this.rest.deleteEmployee(id).subscribe(r => {
      console.log('employee deleted');
    });
  }

}
