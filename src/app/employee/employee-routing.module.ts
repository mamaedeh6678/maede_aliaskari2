import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListComponent} from './list/list.component';
import {RegisterComponent} from './register/register.component';


const routes: Routes = [
  {path: '', component : ListComponent },
  {path: 'list', component : ListComponent },
  {path: 'register', component : RegisterComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
